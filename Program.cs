﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;
using TwitchBotBase.Twitch.Bot;

namespace TwitchBotTests
{
    internal static class Program
    {
        public static void Main()
        {
            dynamic config = JObject.Parse(File.ReadAllText("config.json"));
            if (config == null) throw new InvalidOperationException("Failed parsing config.json");

            TwitchBot twitch = new TwitchBot();

            twitch.Connect((string) config.Username, (string) config.Oauth);
            twitch.ChatMessageReceived += (s, e) => Console.WriteLine($"|{e.Message.Channel}| <{e.Message.Username}> {e.Message.Text}");
            twitch.ChannelJoined += (s, e) => Console.WriteLine($"Joined: {e.Channel}");
            twitch.ChannelLeave += (s, e) => Console.WriteLine($"Left: {e.Channel}");

            twitch.JoinChannel("ssstormy");
            twitch.JoinChannel("discordrelay");

            twitch.Wait();
        }
    }
}
